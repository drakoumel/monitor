
var express      = require('express');
var port         = process.env.PORT || 2343;
var host         = process.env.HOST;
var app          = express();
var bot          = require('./lib/bot.js');
var jid          = process.env.JID || 'userid@chat.hipchat.com';

var chatOptions =
{
  jid: process.env.JID || jid,
  password: process.env.Padssword || 'password',
  host: 'chat.hipchat.com'
}

app.set('port', port);
app.set('host', host);

app.listen(app.get('port'), function(){
  console.log('Its time to pump up the jam! @ ' + ':' + app.get('port'));
  new bot.Bot(chatOptions).connect();
});

