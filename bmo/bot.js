var events        = require('events');
var xmppClient    = require('node-xmpp-client');
var xmppComponent = require('node-xmpp-component');
var xmppJID       = require('node-xmpp-jid');
var bind          = require('underscore').bind;
var once          = require('underscore').once;

var jabber = {};

var Bot = function(options) {


	this.jabber = null;
	this.keepalive = null;
	this.name = null;
	this.plugs = {};
	this.iq_count = 1;

	//once('connect',function() {});

	this.jid = options.jid;
	this.password = options.password;
	this.host = options.host;

	this.connect = connect;
	this.message = message;
	this.getProfile = getProfile;
	this.sendIq = sendIq;
	this.onOnline = onOnline;
	this.setAvailability = setAvailability;

}

var connect = function() {
	console.log('Connecting client...');
	this.jabber = new xmppClient.Client({
jid: this.jid,
password: this.password,
host: this.host
});

//this.jabber.on('error', bind(onStreamError, this));
this.jabber.on('online', bind(onOnline, this));
this.jabber.on('connect', function(params){ });
this.jabber.on('stanza', function(stanza) { console.log(stanza); });
//this.jabber.on('message', function(channel, from, message) { console.log(channel, from, message); });
};


var onMessage = function(condition, callback) {
	if (arguments.length === 1) {
		return this.on('message', condition);
	}

	this.on('message', function(channel, from, message) {
			if (typeof condition === 'string' && message === condition) {
			callback.apply(this, arguments);
			} else if (condition instanceof RegExp) {
			var matches = message.match(condition);

			if (!matches) {
			return;
			}

			var args = Array.prototype.slice.apply(arguments);
			args.push(matches);

			callback.apply(this, args);
			}
			});
};



var serializer = function (data) {
	this.name = data.name;
	this.attrs = data.attrs;
	this.children = data.children;
	this.message = null;
	this.who = null;

	console.log(data.attrs.type);

	if(this.name !== 'message') {
		return;
	}

	if (this.children !== undefined &&
			this.children.length > 0 &&
			this.children[0].name !== undefined &&
			this.children[0].name === 'body')
	{
		this.children.forEach(function (child) {
				if (child.attrs !== undefined && child.name === 'body' && child.children.length !== 0 && child.children[0].length > 0) {
				this.message = child.children[0];
				this.who = data.attrs.from;
				}
				});
	}

	if(this.message) {
		console.log(this.message,this.who);
	}

}

var message = function(targetJid, message) {
	var packet;
	var jid = new xmppJID(targetJid);

	if (jid.domain === this.mucHost) {
		packet = new xmppComponent.Element('message', {
to: targetJid + '/' + this.name,
type: 'groupchat'
});
} else {
	packet = new xmppComponent.Element('message', {
to: targetJid,
type: 'chat',
from: this.jid
});
packet.c('inactive', { xmlns: 'http://jabber/protocol/chatstates' });
}

packet.c('body').t(message);
this.jabber.send(packet);
};


var onStreamError = function(error) {
	if (error instanceof xmppComponent.Element) {
		var condition = error.children[0].name;
		var text = error.getChildText('text');
		if (!text) {
			text = "No error text sent by HipChat, see "
				+ "http://xmpp.org/rfcs/rfc6120.html#streams-error-conditions"
				+ " for error condition descriptions.";
		}

		this.jabber.emit('error', condition, text, error);
	} else {
		this.jabber.emit('error', null, null, error);
	}
};

var onOnline = function() {
	var self = this;
	this.setAvailability('chat');

	this.keepalive = setInterval(function() {
			self.jabber.send(new xmppComponent.Element('message', {}));
			self.jabber.emit('ping');
			}, 30000);

	this.getProfile(function(err, data) {
			if (err) {
			self.jabber.emit('error', null, 'Unable to get profile info: ' + err, null);
			return;
			}

			self.name = data.fn;
			self.mention_name = data.nickname;
			});

	console.log('Connected.');
};

var setAvailability = function(availability, status) {
	var packet = new xmppComponent.Element('presence', { type: 'available' });
	packet.c('show').t(availability);

	if (status) {
		packet.c('status').t(status);
	}

	packet.c('c', {
xmlns: 'http://jabber.org/protocol/caps',
node: 'http://hipchat.com/client/bot', // tell HipChat we're a bot
ver: this.caps_ver
});

this.jabber.send(packet);
};

var getProfile = function(callback) {
	var stanza = new xmppComponent.Element('iq', { type: 'get' })
		.c('vCard', { xmlns: 'vcard-temp' });
	this.sendIq(stanza, function(err, response) {
			var data = {};
			if (!err) {
			var fields = response.getChild('vCard').children;
			fields.forEach(function(field) {
				data[field.name.toLowerCase()] = field.getText();
				});
			}
			callback(err, data, response);
			});
};

var sendIq = function(stanza, callback) {
	stanza = stanza.root(); // work with base element
	var id = this.iq_count++;
	stanza.attrs.id = id;
	once('iq:' + id, callback);
	this.jabber.send(stanza);
};

module.exports.Bot = Bot;



