// server.js

// set up ======================================================================
// get all the tools we need
var express      = require('express');
var routes       = require('routes');
var favicon      = require('serve-favicon');
var session      = require('express-session');
var bodyParser   = require('body-parser');
var http         = require('http');
var multer 	 	 = require('multer');
var errorHandler = require('errorhandler');
var port     	 = process.env.PORT || 8000;
var mongoose 	 = require('mongoose');
var flash    	 = require('connect-flash');
var path     	 = require('path');
var cookieParser = require('cookie-parser')
var morgan       = require('morgan');
var methodOverride = require('method-override');
var routes = require('./routes');

var app      	 = express();

// configuration ===============================================================
//mongoose.connect('mongodb://test:test@lennon.mongohq.com:10070/app24481810');
// connect to our database

// Here we find an appropriate database to connect to, defaulting to
// localhost if we don't find one.
var uristring =
process.env.MONGOLAB_URI ||
process.env.MONGOHQ_URL ||
//'mongodb://heroku:heIb8UJ_xgKPbUK_m_2ja5Ip5dX1WEjupzSLCxErBlEbsLoCfMDzwJZc0JB7sy7eTZX__pmwe3WWr-k8yGx4Eg@lennon.mongohq.com:10017/app32012291';
'mongodb://heroku_app36367050:h3dv5604jdquqgl4vj3jodboq0@ds055990.mongolab.com:55990/heroku_app36367050';

// Makes connection asynchronously.  Mongoose will queue up database
// operations and release them when the connection is complete.
mongoose.connect(uristring, function (err, res) {
  if (err) {
  console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
  console.log ('Succeeded connected to: ' + uristring);
  }
});

// all environments
app.set('view engine','ejs');
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(morgan('dev'));
app.use(methodOverride());
app.use(session({ resave: true,
                  saveUninitialized: true,
                  secret: 'uwotm8' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(multer());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', routes.index);
app.get('/acknowledgement', routes.acknowledgement);
app.get('/api/widget/', routes.apiGetWidget);
app.get('/api/getAllWidgetsForUser/', routes.apiGetAllWidgets);
app.post('/api/postWidget/', routes.apiPostWidget);

// error handling middleware should be loaded after the loading the routes
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

// launch ======================================================================
app.listen(app.get('port'), function(){
  console.log('The magic happens on port ' + app.get('port'));
});
