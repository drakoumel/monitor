// modules/widget.js

var Widget = require('../models/widget');

function handleWidget(id)
{
  // all work related to a widget
  // load it
  // parse it
  // gather data
  return true;
}



// EXAMPLE QUERY
exports.exampleData = function (req,res){
	TW.find()
	  .limit(10)
	  .exec(function(err,tws){
	  	res.send(tws);
	  });
}

// EXAMPLE QUERY
exports.top10 = function (req,res){
	  TW.aggregate(
	  [
	    { $unwind : "$hashstat" },
	    { $group : { _id : "$_id", hashstat : { $sum : 1 } } },
	    { $sort : { hashstat : -1 } },
	    { $limit : 10 }
	  ]).exec(function(err,tws){
	  	if(err){
	  		res.send(err);
	  	}
	  	else{
	  		res.send(tws);
	  	}
	  });
}
