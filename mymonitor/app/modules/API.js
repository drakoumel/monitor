// modules/API.js

var Widget = require('../models/widget');
var WidgetHandler = require('../modules/widget');

exports.testFunction = function (req,res) {
    res.end(JSON.stringify({ data: 0, message:'acknowledged', code: 10 }));
}

exports.request = function (req,res) {
    //get params from req.
    // var id = req.data....;

    //handle the widget data (decide if its sql,new relic etc) -- Here we have the ability to send the SQL processing to another process in case we have heavy queries
    //send it back!
    // the data will be json and angular will handle the visualization

    var result = WidgetHandler.handleWidget(id);
    if(result){
      res.end(JSON.stringify({ data: result, message:'success', code: 10 }));
    }
    else {
      res.end(JSON.stringify({ data: null, message:'fail', code: -1 }));
    }
}

exports.getWidget = function (req, res) {
    var formData = req.body;
    var urlQuery = req.query;

    if(urlQuery.APIKey == (process.env.APIKey || 123)){
      process.nextTick(function () {
              Widget
              .find({ "_id": urlQuery.widgetID, "metadata.authorID" : urlQuery.authorID })
              .exec(function (err, widget) {
                  if (err)
                    res.end(JSON.stringify({ urlQuery: urlQuery, formData: formData, data: null, message: "There was an unexpected error!", code: -1 }));
                  res.end(JSON.stringify({ urlQuery: urlQuery, formData: formData, data: widget, message:'acknowledged', code: 10 }));
              })
      });
    }
    else
      res.end(JSON.stringify({ urlQuery: urlQuery, formData: formData, data: null, message:'Denied', code: -1 }));
}

exports.postWidget = function (req, res) {
    var formData = req.body;
    var urlQuery = req.query;

    if(urlQuery.APIKey == (process.env.APIKey || 123)){
      var newWidget = new Widget();
      newWidget.name = formData.name;
      newWidget.type = formData.type;
      newWidget.metadata.authorID = formData.authorID;
      newWidget.metadata.updated_at = new Date();

      newWidget.save(function (err) {
          if (err)
              res.end(JSON.stringify({ urlQuery: urlQuery, formData: formData, message: "There was an unexpected error!", code: -2 }));
          res.end(JSON.stringify({ urlQuery: urlQuery, formData: formData, message:'acknowledged', code: 10 }));
      });
    }
    else
      res.end(JSON.stringify({ urlQuery: urlQuery, formData: formData, message:'Denied', code: -1 }));
}

exports.getAllWidgetsForUser = function (req, res) {
    var urlQuery = req.query;
    console.log(urlQuery);

    //find all widget ids

    widgetIdArray = [1,2,3,4,5,6,7];

    if(widgetIdArray) {
      res.end(JSON.stringify({ data: widgetIdArray, code: 10 }));
    }
    else {
      res.end(JSON.stringify({ msg: "There was an unexpected error!", code: -1 }));
    }

    process.nextTick(function () {
            Widget
            .find({})
            .sort('-date')
            .exec(function (err, widgets) {
                if (err)
                    res.end(JSON.stringify({ msg: "There was an unexpected error!", code: -1 }));
                if (widgets) {
                    res.end(JSON.stringify({ data: widgets, code: 10 }));
                }
            })
    });
}
