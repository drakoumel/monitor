// models/widget.js

var mongoose = require('mongoose');

var schema = new mongoose.Schema({
   name: { type: String, default: null },
   metadata: {
        created_at: { type: Date, default: Date.now },
        updated_at: { type: Date, default: null },
        //authorID: { type: String, default: null, ref: 'User' }
        authorID: { type: String, default: null }
   },
   type: { type: String, default: null },
   cache: {
     data: { type: String, default: null},
     updated_at:{ type: Date, default: null },
   }
});

module.exports = Widget = mongoose.model('widget', schema);
