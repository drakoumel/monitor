var API = require('./app/modules/API');

module.exports = {
  index: function(req, res) {
  	  res.render('index.ejs', { title: 'MyMonitor' });
    },
  acknowledgement: function(req, res){
    API.testFunction(req,res);
    },
  apiPostWidget: function(req, res) {
    API.postWidget(req, res);
  },
  apiGetAllWidgets: function(req, res) {
    API.getAllWidgetsForUser(req, res);
  },
  apiGetWidget: function(req, res) {
    API.getWidget(req, res);
  }
}
